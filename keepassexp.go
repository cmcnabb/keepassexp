package main

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"os"
	"strings"
	"syscall"
	"time"

	"github.com/hashicorp/vault/api"
	"github.com/tobischo/gokeepasslib"
	"golang.org/x/crypto/ssh/terminal"
)

var keyFile string
var password string
var keepassFile string
var rootGroup string
var outFormat string
var helpFlag bool
var debugFlag bool
var vaultEngine string

func walkGroup(g gokeepasslib.Group, r string, t bool, o string) {
	if strings.TrimSpace(r) == strings.TrimSpace(g.Name) || r == "" {
		t = true
	}
	if t {
		switch o {
		case "json":
			b, err := json.MarshalIndent(g, "", "  ")
			if err != nil {
				fmt.Printf("%s\n", err)
				os.Exit(-1)
			}
			fmt.Printf("%s\n", b)
		case "xml":
			b, err := xml.MarshalIndent(g, "", "  ")
			if err != nil {
				fmt.Printf("%s\n", err)
				os.Exit(-1)
			}
			fmt.Printf("%s\n", b)
		case "text":
			fmt.Printf("Group: %s\n", g.Name)
			for _, e := range g.Entries {
				uuid, _ := e.UUID.MarshalText()
				fmt.Printf("  uuid: %s\n", uuid)
				for _, v := range e.Values {
					fmt.Printf("      %s:  %s\n", v.Key, v.Value.Content)
				}
			}
			if g.Groups != nil {
				for _, gg := range g.Groups {
					walkGroup(gg, r, t, o)

				}
			}
		case "list":
			/*
				for _, e := range g.Entries {
					for _, v := range e.Values {
						if v.Key == "Title" || v.Key == "UserName" {
							fmt.Printf("%s: %s ", v.Key, v.Value.Content)
						}
					}
					fmt.Printf("\n")
				}
				if g.Groups != nil {
					for _, gg := range g.Groups {
						walkGroup(gg, r, t, o)
					}
				}
			*/
			listKeys(g, "")
		case "vault":
			c, err := api.NewClient(&api.Config{
				Address: os.Getenv("VAULT_ADDR"),
			})
			if err != nil {
				fmt.Printf("Failed to create Vault client: %v", err)
				return
			}
			loadVault(c, g, vaultEngine, "")
		default:
			fmt.Printf("Unknown Format: %s\n", o)
			os.Exit(-1)
		}
	}
}

func loadVault(c *api.Client, g gokeepasslib.Group, e string, p string) {
	var sn string
	var vctime time.Time
	var lmtime time.Time
	idate := time.Now().Format("2006-01-02T15:04:05-0700")
	if p == "" && e != "" {
		p = e + "/data"
	}
	if g.Notes != "" {
		s := make(map[string]interface{})
		m := make(map[string]interface{})
		m["description"] = g.Notes
		s["data"] = m
		_, err := c.Logical().Write(p+"/description", s)
		if err != nil {
			fmt.Printf("Error writing %s: %v\n", p, err)
		}
	}
	for _, n := range g.Entries {
		s := make(map[string]interface{})
		m := make(map[string]interface{})
		for _, v := range n.Values {
			if v.Value.Content != "" {
				if v.Key == "Title" {
					sn = strings.ToLower(strings.Replace(v.Value.Content, " ", "_", -1))
				} else {
					m[strings.Replace(strings.ToLower(v.Key), " ", "_", -1)] = v.Value.Content
				}
			}
			m["import_date"] = idate
		}
		s["data"] = m
		ps := p + "/" + sn
		vv, vverr := c.Logical().Read(ps)
		if vverr != nil {
			fmt.Printf("%s", vverr)
			continue
		}
		if vv != nil {
			vctime, _ = time.Parse("2006-01-02T15:04:05.999999999Z07:00", vv.Data["metadata"].(map[string]interface{})["created_time"].(string))
		} else {
			vctime, _ = time.Parse("2006-01-02", "0001-01-01")
		}
		lmtime = n.Times.LastModificationTime.Time
		if lmtime.Before(vctime) {
			fmt.Printf("Keepass %s Last Modification Time %v is older than Vault Creation Time %v.  Skipping\n", ps, lmtime, vctime)
		} else {
			fmt.Printf("Writing %s\n", ps)
			_, err := c.Logical().Write(ps, s)
			if err != nil {
				fmt.Printf("Error writing %s: %v\n", ps, err)
			}
		}
	}
	for _, gg := range g.Groups {
		loadVault(c, gg, "", p+"/"+gg.Name)
	}
}

func listKeys(g gokeepasslib.Group, p string) {
	var sn string
	for _, n := range g.Entries {
		sn = ""
		for _, v := range n.Values {
			if v.Key == "Title" {
				if v.Value.Content != "" {

					sn = strings.ToLower(strings.Replace(v.Value.Content, " ", "_", -1))
					sn = strings.ToLower(strings.Replace(sn, ",", ";", -1))
				}
			}
		}
		if sn == "" {
			sn = "<UNKNOWN>"
		}
		for _, v := range n.Values {
			if v.Key == "UserName" {
				un := strings.Replace(v.Value.Content, ",", ";", -1)
				fmt.Printf("%s/%s,%s\n", p, sn, un)
			}
		}
	}
	for _, gg := range g.Groups {
		listKeys(gg, p+"/"+gg.Name)
	}
}
func main() {
	var err error
	flag.StringVar(&keyFile, "keyfile", "", "Encryption Key File")
	flag.StringVar(&password, "password", "", "Password for the Keepass file")
	flag.StringVar(&keepassFile, "keepassfile", "keepass.kdbx", "Keepass file")
	flag.StringVar(&rootGroup, "root", "", "Group Name to Start With")
	flag.StringVar(&outFormat, "format", "json", "Output Format")
	flag.BoolVar(&helpFlag, "help", false, "Detailed Informatin on Usage")
	flag.StringVar(&vaultEngine, "engine", "", "Vault Secret Engine")
	flag.BoolVar(&debugFlag, "debug", false, "Display debugging info on StdErr")
	flag.Parse()
	if helpFlag {
		fmt.Println("keepassexp - Export a keepass database in one of several formats")
		fmt.Println("  Usage:")
		fmt.Println("    -password <password>")
		fmt.Println("         The password for the keepass db.  This can also be put in")
		fmt.Println("         the KP_PASSWD environment variable or entered at the password")
		fmt.Println("         prompt if not provided on the command line or in the environment")
		fmt.Println("    -keyfile <filenane>")
		fmt.Println("         Path to the file containing the decryption key for the keepass db")
		fmt.Println("    -keepassfile [filename]")
		fmt.Println("         Path to the keepass database file")
		fmt.Println("    -root [group]")
		fmt.Println("         Export this group and all subgroups beneath it")
		fmt.Println("    -format <(json|xml|base64|text|list|vault)>")
		fmt.Println("         Use the selected format for output data (defaults to json)")
		fmt.Println("    -engine <vault secrets engine>")
		fmt.Println("         Where in Vault to put the secrets")
		os.Exit(0)
	}
	if password == "" {
		pwenv, pwenvexists := os.LookupEnv("KP_PASSWD")
		if !pwenvexists {
			fmt.Printf("Password: ")
			bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
			if err != nil {
				fmt.Printf("Failed to read password: %v", err)
			}
			password = string(bytePassword)
			fmt.Println()
		} else {
			password = pwenv
		}
	}
	file, err := os.Open(keepassFile)
	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(-1)
	}
	db := gokeepasslib.NewDatabase()
	if password != "" && keyFile == "" {
		db.Credentials = gokeepasslib.NewPasswordCredentials(password)
	}
	if password == "" && keyFile != "" {
		db.Credentials, err = gokeepasslib.NewKeyCredentials(keyFile)
	}
	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(-1)
	}
	if password != "" && keyFile != "" {
		db.Credentials, err = gokeepasslib.NewPasswordAndKeyCredentials(password, keyFile)
	}
	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(-1)
	}
	err = gokeepasslib.NewDecoder(file).Decode(db)
	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(-1)
	}
	db.UnlockProtectedEntries()
	for _, g := range db.Content.Root.Groups[0].Groups {
		//	for _, g := range db.Content.Root.Groups {
		topFound := false
		walkGroup(g, rootGroup, topFound, outFormat)
	}
}
