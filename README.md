# keepassexp

Export a Keepass Database into one of several formats


###  Usage:
```
    -password <password>
         The password for the keepass db.  This can also be put in
         the KP_PASSWD environment variable or entered at the password
         prompt if not provided on the command line or in the environment
    -keyfile <filenane>
         Path to the file containing the decryption key for the keepass db
    -keepassfile [filename]
         Path to the keepass database file
    -root [group]
         Export this group and all subgroups beneath it
    -format <(json|xml|text|list|vault)>
	     Use the selected format for output data (defaults to json)
    -engine <vault secrets engine>
         Where in Vault to put the secrets

```
Note:  When exporting to Vault make sure the keepass titles are unique 
within a sub group.  The keepass title will be used as the name of the secret
in vault